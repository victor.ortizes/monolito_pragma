package com.practica.monolito.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonaResponseRest extends ResponseRest{

	private PersonaResponse personaResponse = new PersonaResponse();

}
