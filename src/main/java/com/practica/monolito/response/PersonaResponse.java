package com.practica.monolito.response;

import java.util.List;

import com.practica.monolito.modelo.Persona;

import lombok.Data;

@Data
public class PersonaResponse {
	
	private List<Persona> persona;

}
