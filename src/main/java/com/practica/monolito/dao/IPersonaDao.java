package com.practica.monolito.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.practica.monolito.modelo.Persona;

public interface IPersonaDao extends CrudRepository<Persona, Long>{
	/*
	@Transactional(readOnly = true)
	Optional<Persona> findByname(String nombre);
	Optional<Persona> findBylast(String apellido);*/

}
