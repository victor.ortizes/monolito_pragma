package com.practica.monolito.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practica.monolito.modelo.Persona;
import com.practica.monolito.response.PersonaResponseRest;
import com.practica.monolito.services.IPersonaService;

@RestController
@RequestMapping("/monolito/v1")
public class PersonaRestController {
	
	@Autowired
	private IPersonaService service;
	
	/**
	 * obtener todas las personas
	 * @return
	 */
	@GetMapping("/personas")
	public ResponseEntity<PersonaResponseRest> searchPersonas(){
		
		ResponseEntity<PersonaResponseRest> response = service.search();
		return response;
		
	}
	/**
	 * obtener personas por id
	 * @param id
	 * @return
	 */
	
	@GetMapping("/personas/{id}")
	public ResponseEntity<PersonaResponseRest> searchPersonasById(@PathVariable Long id){
		
		ResponseEntity<PersonaResponseRest> response = service.searchById(id);
		return response;
		
	}
	/**
	 * guardar persona
	 * @param persona
	 * @return
	 */
	@PostMapping("/personas")
	public ResponseEntity<PersonaResponseRest> save(@RequestBody Persona persona ){
		
		ResponseEntity<PersonaResponseRest> response = service.save(persona);
		return response;
		
	}
	
	/**
	 *actualizar personas
	 * @param persona
	 * @param id
	 * @return
	 */
	@PutMapping("/personas/{id}")
	public ResponseEntity<PersonaResponseRest> update(@RequestBody Persona persona, @PathVariable Long id ){
		
		ResponseEntity<PersonaResponseRest> response = service.update(persona, id);
		return response;
		
	}
	
	/**
	 * eliminar personas
	 * @param id
	 * @return
	 */
	@DeleteMapping("/personas/{id}")
	public ResponseEntity<PersonaResponseRest> delete(@PathVariable Long id ){
		
		ResponseEntity<PersonaResponseRest> response = service.delete(id);
		return response;
		
	}
	

}
