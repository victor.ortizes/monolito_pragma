package com.practica.monolito.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.practica.monolito.dao.IPersonaDao;
import com.practica.monolito.modelo.Persona;
import com.practica.monolito.response.PersonaResponseRest;

@Service
public class PersonaServicesImpl implements IPersonaService{
	
	@Autowired
	private IPersonaDao personaDao;

	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<PersonaResponseRest> search() {
		
		PersonaResponseRest response = new PersonaResponseRest();
		
		try {
			
			List<Persona> persona = (List<Persona>) personaDao.findAll();
			
			response.getPersonaResponse().setPersona(persona);
			response.setMetadata("Respuesta ok", "00", "Respuesta exitosa");
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error de consulta" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<PersonaResponseRest> searchById(Long id) {
		
		PersonaResponseRest response = new PersonaResponseRest();
		List<Persona> list = new ArrayList<>();
		
		try {
			
			Optional<Persona> persona = personaDao.findById(id);
			
			if (persona != null){
				
				list.add(persona.get());
				response.getPersonaResponse().setPersona(list);
				response.setMetadata("Respuesta ok", "00", "Persona encontrada");
				
			}else {
				
				response.setMetadata("Respuesta no ok", "-1", "Persona no encontrada");
				return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.NOT_FOUND);
				
			}
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error al consultar por nombre" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}

	@Override
	@Transactional
	public ResponseEntity<PersonaResponseRest> save(Persona persona) {

		PersonaResponseRest response = new PersonaResponseRest();
		List<Persona> list = new ArrayList<>();
		
		try {
			
			Persona personaSave = personaDao.save(persona);
			if(personaSave != null) {
				list.add(personaSave);
				response.getPersonaResponse().setPersona(list);
				response.setMetadata("Respuesta ok", "00", "Persona guardada");
			}else {

				response.setMetadata("Respuesta no ok", "-1", "Persona no guardada");
				return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error al guardar persona" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}

	@Override
	@Transactional
	public ResponseEntity<PersonaResponseRest> update(Persona persona, Long id) {

		PersonaResponseRest response = new PersonaResponseRest();
		List<Persona> list = new ArrayList<>();
		
		try {
			
			Optional<Persona> personasearch = personaDao.findById(id);
			
			if(personasearch.isPresent()) {
				
				personasearch.get().setNombre(persona.getNombre());
				personasearch.get().setApellido(persona.getApellido());;
				personasearch.get().setEdad(persona.getEdad());
				personasearch.get().setGenero(persona.getGenero());
				
				Persona personaUpdate = personaDao.save(personasearch.get());
				
				if(personaUpdate != null) {

					list.add(personaUpdate);
					response.getPersonaResponse().setPersona(list);
					response.setMetadata("Respuesta ok", "00", "Persona actualizada");
				}else {
					
					response.setMetadata("Respuesta no ok", "-1", "Persona no actualizada");
					return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.BAD_REQUEST);
				}
				
			}else {

				response.setMetadata("Respuesta no ok", "-1", "Persona no encontrada");
				return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error al actualizar persona" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}

	@Override
	@Transactional
	public ResponseEntity<PersonaResponseRest> delete(Long id) {

		PersonaResponseRest response = new PersonaResponseRest();
		
		try {
			
			personaDao.deleteById(id);
			response.setMetadata("Respuesta ok", "00", "Persona eliminada");
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error al eliminar persona" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}
	
/*
	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<PersonaResponseRest> searchByLast(String apellido) {
		
		PersonaResponseRest response = new PersonaResponseRest();
		List<Persona> list = new ArrayList<>();
		
		try {
			
			Optional<Persona> persona = personaDao.findBylast(apellido);
			
			if (persona != null){
				
				list.add(persona.get());
				response.getPersonaResponse().setPersona(list);
				response.setMetadata("Respuesta ok", "00", "Persona encontrada");
				
			}else {
				
				response.setMetadata("Respuesta no ok", "-1", "Persona no encontrada");
				return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
				
			}
			
		} catch (Exception e) {

			response.setMetadata("Respuesta no ok", "-1", "Error al consultar por nombre" + e);
			e.getStackTrace();
			return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<PersonaResponseRest>(response, HttpStatus.OK);
	}

	@Override
	@Transactional(readOnly = true)
	public ResponseEntity<PersonaResponseRest> searchByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}
	*/

}
