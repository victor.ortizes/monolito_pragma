package com.practica.monolito.services;

import org.springframework.http.ResponseEntity;

import com.practica.monolito.modelo.Persona;
import com.practica.monolito.response.PersonaResponseRest;

public interface IPersonaService {
	
	public ResponseEntity<PersonaResponseRest> search();
	public ResponseEntity<PersonaResponseRest> searchById(Long id);
	public ResponseEntity<PersonaResponseRest> save(Persona persona);
	public ResponseEntity<PersonaResponseRest> update(Persona persona, Long id);
	public ResponseEntity<PersonaResponseRest> delete(Long id);/*
	public ResponseEntity<PersonaResponseRest> searchByName(String nombre);
	public ResponseEntity<PersonaResponseRest> searchByLast(String apellido);*/

}
